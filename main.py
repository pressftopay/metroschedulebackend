from fastapi import FastAPI

import ParserMetro

app = FastAPI()


@app.get("/")
async def root():
    return {ParserMetro.test}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}
